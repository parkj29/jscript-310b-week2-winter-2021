/*
Name: Joseph Park
NetID: parkj29
JSCRIPT 310B Class HMWK Ex. 2
*/

// 1. Create an object representation of yourself
// Should include: 
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)
const myself = {
  firstName: "Joseph",
  lastName: "Park",
  'favorite food': 'Pizza',
  bestFriend : {firstName: "James", 
                lastName: "Johnson",
                'favorite food': 'soup'}

  }

// 2. console.log best friend's firstName and your favorite food
console.log("my best friend's first name is: " + myself.bestFriend.firstName);
console.log("my favorite food is: " + myself["favorite food"])

// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X
const ticTacToeArray = [['-', 'o', '-'],
                        ['-', 'x','o'],
                        ['x', '-', 'x']]
console.log("BEFORE insert 'o'")                        
console.log(ticTacToeArray[0])
console.log(ticTacToeArray[1])
console.log(ticTacToeArray[2])

// 4. After the array is created, 'O' claims the top right square.
// Update that value.
ticTacToeArray[0][2] = 'o'

// 5. Log the grid to the console.
console.log("AFTER insert 'o'")                        
console.log(ticTacToeArray[0]);
console.log(ticTacToeArray[1]);
console.log(ticTacToeArray[2]);

// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test
const emailAddress = "joePark@gmail.com";
const found = emailAddress.match(/[\w|.|-]*@\w*\.[\w|.]*/g);
console.log(found);
// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date
const assignmentDate = '1/21/2019';
const dateObject = new Date(assignmentDate);

// 8. Create a new Date instance to represent the dueDate.  
// This will be exactly 7 days after the assignment date.
const newDateObject = new Date(assignmentDate);
newDateObject.setDate(newDateObject.getDate() + 7)
console.log(newDateObject)

// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help
const dueDate = new Date(newDateObject)

//'<time datetime="2018-01-14">January 14, 2018</time>'
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];
const timeTag = "<time datetime=\"" + dueDate.getFullYear() + '-' + dueDate.getMonth() + '-' + dueDate.getDay()+"\">" + months[dueDate.getMonth()] +' '+ dueDate.getDay()+ ', ' + dueDate.getFullYear()+'</time>'


// 10. log this value using console.log
console.log(timeTag)
